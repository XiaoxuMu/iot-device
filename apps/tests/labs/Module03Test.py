import unittest
from labs.module03.TempSensorAdaptor import TempSensorAdaptor




"""
Test class for all requisite Module03 functionality.

Instructions:
1) Rename 'testSomething()' function such that 'Something' is specific to your needs; add others as needed, beginning each function with 'test...()'.
2) Import the relevant modules and classes to support your tests.
3) Run this class as unit test app
4) Include a screen shot of the report when you submit your assignment
"""
class Module03Test(unittest.TestCase):

	"""
	Use this to setup your tests. This is where you may want to load configuration
	information (if needed), initialize class-scoped variables, create class-scoped
	instances of complex objects, initialize any requisite connections, etc.
	"""
	def setUp(self):
		print("Start test")
		pass

	"""
	Use this to tear down any allocated resources after your tests are complete. This
	is where you may want to release connections, zero out any long-term data, etc.
	"""
	def tearDown(self):
		print("End test")
		pass

	"""
	Place your comments describing the test here.
	"""
	def test_nominalTemp_reader(self):
		print("Test nominalTemp reader.")
		adaptor = TempSensorAdaptor()
		temp = adaptor.reader()
		self.assertTrue(int(temp)== 20)
		pass

if __name__ == "__main__":
	#import sys;sys.argv = ['', 'Test.testName']
	unittest.main()