import unittest
import labs
from labs.module01.SystemCpuUtilTask import SystemCpuUtilTask
from labs.module01.SystemMemUtilTask import SystemMemUtilTask


"""
Test class for all requisite Module01 functionality.

Instructions:
1) Rename 'testSomething()' function such that 'Something' is specific to your needs; add others as needed, beginning each function with 'test...()'.
2) Import the relevant modules and classes to support your tests.
3) Run this class as unit test app
4) Include a screen shot of the report when you submit your assignment
"""
class Module01Test(unittest.TestCase):

	"""
	Use this to setup your tests. This is where you may want to load configuration
	information (if needed), initialize class-scoped variables, create class-scoped
	instances of complex objects, initialize any requisite connections, etc.
	"""
	def setUp(self):
		print("Start test")
		pass

	"""
	Use this to tear down any allocated resources after your tests are complete. This
	is where you may want to release connections, zero out any long-term data, etc.
	"""
	def tearDown(self):
		print("End test.")
		pass
	
	"""
	test the range of CPU output value
	"""
	def test_cpuoutputvalue(self):
		print("Test CPU utilization percent float value.")
		cpudata = SystemCpuUtilTask.getDataFromSensor(self)
		self.assertTrue(cpudata > 0.0)
		self.assertTrue(cpudata <= 100.0)
		pass
 	
	"""
	test the range of Memory output value
	"""
	def test_memoutputvalue(self):
		print("Test memory utilization percent float value.")
		memdata = SystemMemUtilTask.getDataFromSensor(self)
		self.assertTrue(memdata > 0.0)
		self.assertTrue(memdata <= 100.0)
		pass

if __name__ == "__main__":
	#import sys;sys.argv = ['', 'Test.testName']
	unittest.main()