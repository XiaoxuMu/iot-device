import unittest
from labbenchstudios.common import DataUtil


"""
Test class for all requisite Module05 functionality.

Instructions:
1) Rename 'testSomething()' function such that 'Something' is specific to your needs; add others as needed, beginning each function with 'test...()'.
2) Import the relevant modules and classes to support your tests.
3) Run this class as unit test app
4) Include a screen shot of the report when you submit your assignment
"""
class Module05Test(unittest.TestCase):

	"""
	Use this to setup your tests. This is where you may want to load configuration
	information (if needed), initialize class-scoped variables, create class-scoped
	instances of complex objects, initialize any requisite connections, etc.
	"""
	def setUp(self):
		print("Start test")
		pass

	"""
	Use this to tear down any allocated resources after your tests are complete. This
	is where you may want to release connections, zero out any long-term data, etc.
	"""
	def tearDown(self):
		print("End test")
		pass

	"""
	Place your comments describing the test here.
	"""
	def testReadSensorDataFromJsonFile(self):
		datautil = DataUtil.DataUtil()
		print("Test read Sensor Data From Json File.")
		datautil.toSensorDataFromJsonFile("/Users/Cassie/eclipse-workspace/iot-device/Data/Sensordata.json")
		pass


if __name__ == "__main__":
	#import sys;sys.argv = ['', 'Test.testName']
	unittest.main()