import unittest
from labbenchstudios.common import DataUtil
from labbenchstudios.common import SensorData

"""
Test class for all requisite DataUtil functionality.

Instructions:
1) Rename 'testSomething()' function such that 'Something' is specific to your needs; add others as needed, beginning each function with 'test...()'.
2) Import the relevant modules and classes to support your tests.
3) Run this class as unit test app
4) Include a screen shot of the report when you submit your assignment
"""
class DataUtilTest(unittest.TestCase):

	"""
	Use this to setup your tests. This is where you may want to load configuration
	information (if needed), initialize class-scoped variables, create class-scoped
	instances of complex objects, initialize any requisite connections, etc.
	"""
	def setUp(self):
		print("Start test")
		pass

	"""
	Use this to tear down any allocated resources after your tests are complete. This
	is where you may want to release connections, zero out any long-term data, etc.
	"""
	def tearDown(self):
		print("Start test")
		pass
	
	"""
	Place your comments describing the test here.
	"""
	def testSensorDatajson(self):
		datautil = DataUtil.DataUtil()
		sd = SensorData.TempSensorData()
		print("Test toJsonFromSensorData() fuction.")
		jd = datautil.toJsonFromSensorData(sd.name,sd.timeStamp,sd.avgValue,sd.minValue,sd.maxValue,sd.curValue,sd.totValue,sd.sampleCount)
		print("Test toSensorDataFromJson() fuction.")
		datautil.toSensorDataFromJson(jd)
		print("Test toSensorDataFromJsonFile() fuction.")
		datautil.toSensorDataFromJsonFile("/Users/Cassie/eclipse-workspace/iot-device/Data/Sensordata.json")
		pass

if __name__ == "__main__":
	#import sys;sys.argv = ['', 'Test.testName']
	unittest.main()