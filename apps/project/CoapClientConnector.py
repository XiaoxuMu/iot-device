'''
Created on Oct 24, 2019

@author: Xiaoxu Mu
'''
from coapthon.client.helperclient import HelperClient
from labbenchstudios.common import ConfigConst
from labbenchstudios.common import ConfigUtil
from labbenchstudios.common import DataUtil
from labbenchstudios.common import SensorData
from sense_hat import SenseHat

# CoapClientConnector class is to execute the following requests from the client to the server:
# 1. Ping
# 2. GET
# 3. POST 
# 4. PUT
# 5. DELETE
class CoapClientConnector():
    #set up host, port and path  
    config = ConfigUtil.ConfigUtil('/Users/Cassie/eclipse-workspace/iot-device/config/ConnectedDevicesConfig.props')
    host = config.getProperty(ConfigConst.COAP_CLOUD_SECTION, ConfigConst.HOST_KEY )
    port = int(config.getProperty(ConfigConst.COAP_CLOUD_SECTION, ConfigConst.PORT_KEY))
    path = "sensor"
        
    print('\tHost: ' + str(host))
    print('\tPort: ' + str(port))
        
    url = "coap://" + str(host) + ":" + str(port) + "/sensor"
    print('Coap broker address: ' + str(url))
    
    client = HelperClient(server = (host, port))
    print("Created CoAP client reference: " + str(client))
    
    tempsensorData = SensorData.SensorData()
    humisensorData = SensorData.SensorData()
    presssensorData = SensorData.SensorData()
    
    def __init__(self):
        pass
    
    # Execute GET request to server.
    def handleGetTest(self,resource):
    
        print("\nTesting GET for resource: " + str(resource))
        
        response = self.client.get(resource)
        print(response.pretty_print())
        print("GET COMPLETED!")
    
    # Execute POST request to server.
    def handlePostTest(self, resource, payload):
    
        print("\nTesting POST for resource: " + resource + ", payload: " + payload)

        response = self.client.post(resource, payload)
        print(response.pretty_print())
        print("POST COMPLETED!")
 
    # Execute PUT request to server.    
    def handlePutTest(self, resource, payload):
    
        print("\nTesting PUT for resource: " + resource + ", payload: " + payload)
        
        response = self.client.put(resource, payload)
        print(response.pretty_print())
        print("PUT COMPLETED!")

    # Execute DELETE request to server.    
    def handleDeleteTest(self, resource):
    
        print("\nTesting DELETE for resource: " + resource)
        
        response = self.client.delete(resource)
        print(response.pretty_print())
        print("DELETE COMPLETED!")
        
    #Convert Json to Sensor Data
    def convertJsontoSensordata(self, data):
        
        datautil = DataUtil.DataUtil()
        print("\nConvert Json to SensorData Instance: ")
        sensordata = datautil.toSensorDataFromJson(data)
    
        return sensordata
    
    #Convert Sensor Data to Json
    def convertSensordatatoJson(self, sensorData):

        datautil = DataUtil.DataUtil()
        print("\nConvert SensorData Instance to Json: ")
        jsonsensordata = datautil.toJsonFromSensorData(sensorData.name, sensorData.timeStamp, 
                                  sensorData.avgValue, sensorData.minValue, 
                                  sensorData.maxValue, sensorData.curValue, 
                                  sensorData.totValue, sensorData.sampleCount)
        return jsonsensordata
   
    #Using tempSensorData class and DataUtil class to create a simple tempSensorData instance
    def setTempData(self):
        self.tempsensorData.setName("Temperature")
        dataUtil = DataUtil.DataUtil()
        sensehat = SenseHat()
        temp = sensehat.get_temperature()
        self.tempsensorData.addValue(temp)
        print("\nCreate Json format of TempSensor data:")
        payload = dataUtil.toJsonFromSensorData(self.tempsensorData.name, self.tempsensorData.timeStamp,
                                        self.tempsensorData.avgValue, self.tempsensorData.minValue,
                                        self.tempsensorData.maxValue, self.tempsensorData.curValue,
                                        self.tempsensorData.totValue, self.tempsensorData.sampleCount)  
        return payload   
      
    #Using humiSensorData class and DataUtil class to create a simple humiSensorData instance
    def setHumiData(self):
        self.humisensorData.setName("Humidity")
        dataUtil = DataUtil.DataUtil()
        sensehat = SenseHat()
        humi = sensehat.get_humidity()
        self.humisensorData.addValue(humi)
        print("\nCreate Json format of HumiSensor data:")
        payload = dataUtil.toJsonFromSensorData(self.humisensorData.name, self.humisensorData.timeStamp,
                                        self.humisensorData.avgValue, self.humisensorData.minValue,
                                        self.humisensorData.maxValue, self.humisensorData.curValue,
                                        self.humisensorData.totValue, self.humisensorData.sampleCount)  
        return payload   
    
    #Using pressSensorData class and DataUtil class to create a simple pressSensorData instance
    def setPressData(self):
        self.presssensorData.setName("Pressure")
        dataUtil = DataUtil.DataUtil()
        sensehat = SenseHat()
        press = sensehat.get_pressure()
        self.presssensorData.addValue(press)
        print("\nCreate Json format of PressSensor data:")
        payload = dataUtil.toJsonFromSensorData(self.presssensorData.name, self.presssensorData.timeStamp,
                                        self.presssensorData.avgValue, self.presssensorData.minValue,
                                        self.presssensorData.maxValue, self.presssensorData.curValue,
                                        self.presssensorData.totValue, self.presssensorData.sampleCount)  
        return payload   
      
    
        
        