'''
Created on Oct 23, 2019

@author: Xiaoxu Mu
'''
# This is the Mqtt Subscriber application
from labbenchstudios.common import ConfigConst
from labbenchstudios.common import ConfigUtil
import paho.mqtt.client as mqtt
from labbenchstudios.common import DataUtil
from project import SenseHatLedActivator
from sense_hat import SenseHat

#Define variables
config = ConfigUtil.ConfigUtil('/Users/Cassie/eclipse-workspace/iot-device/config/ConnectedDevicesConfig.props')
host = config.getProperty(ConfigConst.MQTT_CLOUD_SECTION, ConfigConst.HOST_KEY)
port = int(config.getProperty(ConfigConst.MQTT_CLOUD_SECTION, ConfigConst.PORT_KEY))

led = SenseHatLedActivator.SenseHatLedActivator()
datautil = DataUtil.DataUtil()
sensehat = SenseHat()
temp = sensehat.get_temperature()

#Convert data format between Json and SensorData
def convert(data):
    print("\nConvert Json to ActuatorData Instance: ")
    actuatordata = datautil.toActuatorDataFromJson(data)
    actuatorval = actuatordata.getValue()
    print("\nTempActuator = " + str(actuatorval))
    
    # Parse the tempSensorData content to determine if the actual temperature exceeds or falls below
    # ‘actuatordata’ from the mqtt server.
    # val refers to the adjustment of temperature   
    val = temp - actuatorval
    print('\nCurrently the temperature is ' + str(temp))
    if (val > 0):
        print('Decrease the temperature to' + " " + str(actuatorval) + '.')
        actuatordata.command = 1
        actuatordata.statusCode = 1
        actuatordata.stateData = -1
        actuatordata.val = abs(val)
        print('\n' + str(actuatordata.getActuatorData())) 
                    
        led.enableLed = True
        # Refer to the temperature will be lowered by n degree
        led.displayMsg = 'Should decrease ' + " " + str(abs(val)) + " degree. \n"
        led.start()
        led.enableLed = False
    else :
        if (val < 0):
            print('Increase the temperature to' + " " + str(actuatorval) + '.')
            actuatordata.command = 1
            actuatordata.statusCode = 1
            actuatordata.stateData = 1
            actuatordata.val = abs(val)
            print('\n' + str(actuatordata.getActuatorData()))
            
            led.enableLed = True
            # Refer to the temperature will be raised by n degree
            led.displayMsg = 'Should increase ' + " " + str(abs(val)) + " degree. \n"
            led.start()
            led.enableLed = False
        else:
            actuatordata.command = 0
            actuatordata.statusCode = 0
            actuatordata.stateData = 0
            actuatordata.val = 0.0
            print('\n' + str(actuatordata.getActuatorData())) 
            led.enableLed = True
            led.displayMsg = str(temp)
            led.start()
            led.enableLed = False              
    
#Initiate a mqtt client
mqttClient = mqtt.Client() 
 
#Define on_connect event Handler
def on_connect(client, userdata, flags, rc):
    if rc == 0:
        print("Connected to the broker. Returned code = ",rc)
    else:
        print("Connection failed. Returned code = ",rc)
            
#Define on_message event Handler  
def on_message(client, userdata, msg):
    message = str(msg.payload)
    jsondata = message.lstrip("b'").rstrip("'")
    print("\nTopic: " + str(msg.topic) + ". Received message: " + jsondata)
    convert(jsondata)
#     mqttClient.unsubscribe(msg.topic)
    
#Define on_subscribe event Handler  
def on_subscribe(client, userdata, mid, granted_qos):
    print("Subscribed to MQTT topic.")

def on_log(client, userdata, level, buf):
    print("log: ",buf)

# Register event handlers    
mqttClient.on_message = on_message
mqttClient.on_connect = on_connect
mqttClient.on_subscribe = on_subscribe
#mqttClient.on_log = on_log

#Connect with MQTT broker
mqttClient.connect(host, port)
mqttClient.subscribe("TempActuator", qos = 2)

#Continue the network loop
mqttClient.loop_forever()

    
    

        
    
    
    