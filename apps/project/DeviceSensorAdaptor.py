'''
Created on Sep 24, 2019

@author: Xiaoxu Mu
'''

import threading
import time 
from labbenchstudios.common import DataUtil
from project import SmtpClientConnector
from sense_hat import SenseHat
from labbenchstudios.common import ActuatorData
from project import CoapClientConnector

class DeviceSensorAdaptor(threading.Thread):
    
    RATE_PERSEC = 10
    actuatorData = ActuatorData.ActuatorData()
    jsondata = DataUtil.DataUtil()        
    connector = SmtpClientConnector.SmtpClientConnector()
    sensehat = SenseHat()
    threshold = 2
    
    # constants used to display an up and down arrows plus bars
    # set up the colours (blue, red, empty)
    b = [0, 0, 255]  # blue
    r = [255, 0, 0]  # red
    e = [0, 0, 0]  # empty
    # create images for up and down arrows
    arrow_up = [
        e, e, e, r, r, e, e, e,
        e, e, r, r, r, r, e, e,
        e, r, e, r, r, e, r, e,
        r, e, e, r, r, e, e, r,
        e, e, e, r, r, e, e, e,
        e, e, e, r, r, e, e, e,
        e, e, e, r, r, e, e, e,
        e, e, e, r, r, e, e, e
    ]
    
    arrow_down = [
        e, e, e, b, b, e, e, e,
        e, e, e, b, b, e, e, e,
        e, e, e, b, b, e, e, e,
        e, e, e, b, b, e, e, e,
        b, e, e, b, b, e, e, b,
        e, b, e, b, b, e, b, e,
        e, e, b, b, b, b, e, e,
        e, e, e, b, b, e, e, e
    ]
    
    bars = [
        e, e, e, e, e, e, e, e,
        e, e, e, e, e, e, e, e,
        r, r, r, r, r, r, r, r,
        r, r, r, r, r, r, r, r,
        b, b, b, b, b, b, b, b,
        b, b, b, b, b, b, b, b,
        e, e, e, e, e, e, e, e,
        e, e, e, e, e, e, e, e
    ]

    # DeviceManagementApp connects to the CoAP server using CoapClientConnector,run a number of tests, then exit.
    Coapclient = CoapClientConnector.CoapClientConnector()
    curVal = Coapclient.tempsensorData.getValue()
 
    def __init__(self, rate = RATE_PERSEC):
        super(DeviceSensorAdaptor, self).__init__()
        self.rate = rate
        
    # A thread to get temperature, humidity and pressure value from sensor 
    # and send the generated SensorData content as JSON.
    # Write the generated SensorData content as JSON to a file to the file system.
    def run(self):
        while True:
            if self.enableAdaptor:
                
                
                #set json payload of resource "temp".
                payload = self.Coapclient.setTempData()
                #Execute POST request.
                self.Coapclient.handlePostTest("temp", payload)
                
                self.getSensorData()
                self.compare()
                self.sendNotification()

                #set json payload of resource "humi".
                payload = self.Coapclient.setHumiData()
                #Execute POST request.
                self.Coapclient.handlePostTest("humi", payload)
                
                #set json payload of resource "press".
                payload = self.Coapclient.setPressData()
                #Execute POST request.
                self.Coapclient.handlePostTest("press", payload)
                
            time.sleep(self.rate)
                   
    def getSensorData(self):
        return self.Coapclient.tempsensorData.__str__()
    
    #Sense HAT displays a red arrow pointing up when the temperature increased over the average readings, 
    #a blue arrow pointing down when the temperature decreased below the average readings, 
    #and an equal sign when the temperature stays the same between current value and average value.
    def compare(self): 
        if (self.Coapclient.tempsensorData.getValue() - self.Coapclient.tempsensorData.getAvgValue() > 0):
            self.sensehat.set_pixels(self.arrow_up)
        else:
            if (self.Coapclient.tempsensorData.getValue() - self.Coapclient.tempsensorData.getAvgValue() < 0):
                print('\n  Current temp is below average by < ' + str(self.threshold) + '. Sending data...')
                self.sensehat.set_pixels(self.arrow_down)
            else:
                self.sensehat.set_pixels(self.bars)
                           
    # Check if the value read is different from the average stored measurement by a configurable threshold.
    # If the threshold is reached and surpassed, the most recent SensorData should be e-mailed to a remote service 
    # using the SmtpClientConnector module.   
    def sendNotification(self):
        if (self.Coapclient.tempsensorData.getValue() - self.Coapclient.tempsensorData.getAvgValue() > self.threshold):
            print('\n  Current temp exceeds average by > ' + str(self.threshold) + '. Sending data...')
            self.connector.publishMessage("Temperature From Sensor", '\n  Current temp exceeds average by > ' + str(self.threshold) + '.\n' +
                                          self.jsondata.toJsonFromSensorData(self.Coapclient.tempsensorData.name,self.Coapclient.tempsensorData.timeStamp, 
                                                                     self.Coapclient.tempsensorData.getAvgValue(),self.Coapclient.tempsensorData.getMinValue(),
                                                                     self.Coapclient.tempsensorData.getMaxValue(),self.Coapclient.tempsensorData.getValue(),
                                                                     self.Coapclient.tempsensorData.totValue,self.Coapclient.tempsensorData.sampleCount))
        else:
            if (self.Coapclient.tempsensorData.getAvgValue() - self.Coapclient.tempsensorData.getValue() > self.threshold):
                print('\n  Current temp is below average by < ' + str(self.threshold) + '. Sending data...')
                self.connector.publishMessage("Temperature From Sensor", '\n  Current temp is below average by < ' + str(self.threshold) + '.\n ' + 
                                              self.jsondata.toJsonFromSensorData(self.Coapclient.tempsensorData.name,self.Coapclient.tempsensorData.timeStamp, 
                                                                     self.Coapclient.tempsensorData.getAvgValue(),self.Coapclient.tempsensorData.getMinValue(),
                                                                     self.Coapclient.tempsensorData.getMaxValue(),self.Coapclient.tempsensorData.getValue(),
                                                                     self.Coapclient.tempsensorData.totValue,self.Coapclient.tempsensorData.sampleCount))
            else:
                print('\n  Current temp = ' + str(self.Coapclient.tempsensorData.getValue()))