'''
Created on Sep 24, 2019

@author: Xiaoxu Mu
'''
#SmtpClientConnector class implements an SMTP send with text data to an e-mail address.
#It be configured via the data stored in ConfigUtil from the ConnectedDevicesConfig.props. 

from labbenchstudios.common import ConfigConst
from labbenchstudios.common import ConfigUtil
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText 
 

class SmtpClientConnector(object):
    
    def __init__(self):
        self.config = ConfigUtil.ConfigUtil('/Users/Cassie/eclipse-workspace/iot-gateway/config/ConnectedDevicesConfig.props')
        self.config.loadConfig()
    
    #Configure the information in email format    
    def publishMessage(self, topic, data):
        host = self.config.getProperty(ConfigConst.SMTP_CLOUD_SECTION, ConfigConst.HOST_KEY)
        port = self.config.getProperty(ConfigConst.SMTP_CLOUD_SECTION, ConfigConst.PORT_KEY)
        fromAddr = self.config.getProperty(ConfigConst.SMTP_CLOUD_SECTION, ConfigConst.FROM_ADDRESS_KEY)
        toAddr = self.config.getProperty(ConfigConst.SMTP_CLOUD_SECTION, ConfigConst.TO_ADDRESS_KEY)
        authToken = self.config.getProperty(ConfigConst.SMTP_CLOUD_SECTION, ConfigConst.USER_AUTH_TOKEN_KEY)
        msg = MIMEMultipart()
        msg['From'] = fromAddr
        msg['To'] = toAddr
        msg['Subject'] = topic 
        msgBody = str(data)

        msg.attach(MIMEText(msgBody)) 
        msgText = msg.as_string()
        
        # send e-mail notification
        smtpServer = smtplib.SMTP_SSL(host, port) 
        smtpServer.ehlo()
        smtpServer.login(fromAddr, authToken) 
        smtpServer.sendmail(fromAddr, toAddr, msgText) 
        smtpServer.close()
        