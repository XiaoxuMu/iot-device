'''
Created on Sep 24, 2019

@author: Xiaoxu Mu
'''

# To start the thread.
import logging 
from time import sleep        
from project import DeviceSensorAdaptor

logging.info("Starting data processing app daemon thread...")

#Sending sensor data from device to gateway
devSenAdaptor = DeviceSensorAdaptor.DeviceSensorAdaptor()
devSenAdaptor.enableAdaptor = True
devSenAdaptor.start()
 
while (True):
    sleep(10)
    pass

