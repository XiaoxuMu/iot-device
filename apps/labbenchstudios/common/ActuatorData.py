'''
Created on Sep 30, 2019

@author: Xiaoxu Mu
'''
#ActuatorData Class stores the aggregated actuator data 
#track the updates for time, error code, status code value. 
import os
from datetime import datetime

COMMAND_OFF = 0
COMMAND_ON = 1
COMMAND_SET = 2
COMMAND_RESET = 3

STATUS_IDLE   = 0
STATUS_ACTIVE = 1

ERROR_OK =0 
ERROR_COMMAND_FAILED = 1 
ERROR_NON_RESPONSIBLE = -1

class ActuatorData():
    name = 'Actuator Data'
    hasError = False
    command = 0
    errCode = 0
    statusCode = 0
    stateData = None
    val = 0.0

    def __init__(self):
        self.updateTimeStamp()
        
    def updateData(self, data):
        self.command    = data.getCommand()
        self.statusCode = data.getStatusCode()
        self.errCode    = data.getErrorCode()
        self.stateData  = data.getStateData()
        self.val        = data.getValue()
        
    def updateTimeStamp(self):
        self.timeStamp = str(datetime.now())
        
    def getCommand(self):
        return self.command
   
    def getStatusCode(self):
        return self.statusCode
   
    def getErrorCode(self):
        return self.errCode
    
    def getStateData(self):
        return self.stateData
    
    def getValue(self):
        return self.val
    
    def pringActuatorData(self):
        print("\nNew actuator readings: \n" + "  name=" + self.name + 
              ",timeStamp="+ self.timeStamp + ",command=" + str(self.getCommand()) + 
              ",errCode=" + str(self.getErrorCode()) + ",statusCode=" + str(self.getStatusCode()) +
              ",stateData=" + str(self.getStateData())+ ",val=" + str(self.getValue()))
        
    def __str__(self):           
        customStr = \
            str(self.name + ':' + \
            os.linesep + '\tCommand:    ' + str(self.command) + \
            os.linesep + '\tStatusCode: ' + str(self.statusCode) + \
            os.linesep + '\tErrorCode:  ' + str(self.errCode) + \
            os.linesep + '\tStateData:  ' + str(self.stateData) + \
            os.linesep + '\tValue:      ' + str(self.val))    
        return customStr
    
    def getActuatorData(self):
        return self.__str__()
        
    