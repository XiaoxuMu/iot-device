'''
Created on Oct 14, 2019

@author: Xiaoxu Mu
'''
import json
from labbenchstudios.common import ActuatorData
from labbenchstudios.common import SensorData

# DataUtil class is used to convert to JSON from SensorData and vice versa, 
# as well as to JSON from ActuatorData, and vice versa.

class DataUtil():
    
    #Accepts a SensorData reference as a parameter, converts it to JSON, and returns the String.
    def toJsonFromSensorData(self,name,timeStamp,avgValue,minValue,maxValue,curValue,totValue,sampleCount):
        json_info = {}
        json_info['name'] = name
        json_info['timeStamp'] = timeStamp
        json_info['avgValue'] = avgValue
        json_info['minValue'] = minValue
        json_info['maxValue'] = maxValue
        json_info['curValue'] = curValue
        json_info['totValue'] = totValue
        json_info['sampleCount'] = sampleCount
        
        #write Json to a file
        with open("/Users/Cassie/eclipse-workspace/iot-device/Data/Sensordata.json","w+") as outfile:
            json.dump(json_info,outfile)
            
        # write data into json file   
        jsonData = json.dumps(json_info)

#         print("\n From SensorData to Json: ")
        print(jsonData)
        return jsonData
    
    #Accepts a JSON String as a parameter, converts it to a SensorData instance, 
    #and returns the SensorData object.
    def toSensorDataFromJson(self, jsonData):
        sdDict = json.loads(jsonData)
        #print(" decode [pre]  --> " + str(sdDict))
        sd = SensorData.SensorData()
        sd.name = sdDict['name']
        sd.timeStamp = sdDict['timeStamp']
        sd.avgValue = sdDict['avgValue']
        sd.minValue = sdDict['minValue']
        sd.maxValue = sdDict['maxValue']
        sd.curValue = sdDict['curValue']
        sd.totValue = sdDict['totValue']
        sd.sampleCount = sdDict['sampleCount']
        #print(" decode [post] --> " + str(sd))
#         print("\n From Json to SensorData: ")
        print(sd)
        return sd
    
    # Accepts a String filename and path as a parameter. 
    # It points to a JSON file that can be read and used to generate a SensorData instance. 
    def toSensorDataFromJsonFile(self,path):
        with open(path,"r") as json_file:
            sdDict = json.load(json_file)
            sd = SensorData.SensorData()
            sd.name = sdDict['name']
            sd.timeStamp = sdDict['timeStamp']
            sd.avgValue = sdDict['avgValue']
            sd.minValue = sdDict['minValue']
            sd.maxValue = sdDict['maxValue']
            sd.curValue = sdDict['curValue']
            sd.totValue = sdDict['totValue']
            sd.sampleCount = sdDict['sampleCount']
            
            print("\n From Json File to SensorData: ")
            print (sd)
            return sd
        
    #Accepts a ActuatorData reference as a parameter, converts it to JSON, and returns the String.
    def toJsonFromActuatorData(self,name,timeStamp,command,errCode,statusCode,stateData,val):
        json_info = {}
        json_info['name'] = name
        json_info['timeStamp'] = timeStamp
        json_info['command'] = command
        json_info['errCode'] = errCode
        json_info['statusCode'] = statusCode
        json_info['stateData'] = stateData
        json_info['val'] = val
        
        #write Json to a file
        with open("/Users/Cassie/eclipse-workspace/iot-device/Data/Actuatordata.json","w+") as outfile:
            json.dump(json_info,outfile)
        # write data into json file
        jsonData = json.dumps(json_info)
        
        print("\n From ActuatorData to Json: ")
        print(jsonData)
        return jsonData
        
    #Accepts a JSON String as a parameter, converts it to a ActuatorData instance,
    #and returns the ActuatorData object.      
    def toActuatorDataFromJson(self, jsonData):
        adDict = json.loads(jsonData)
        #print(" decode [pre]  --> " + str(adDict))
        ad = ActuatorData.ActuatorData()
        ad.name = adDict['name']
#         ad.timeStamp = adDict['timeStamp']
        ad.command = adDict['command']
        ad.errCode = adDict['errCode']
        ad.statusCode  = adDict['statusCode']
        ad.stateData   = adDict['stateData']
        ad.val    = adDict['val']
        
        #print(" decode [post] --> " + str(ad))
#         print("\n From Json to ActuatorData: ")
        print(ad)
        return ad
   
    # Accepts a String filename and path as a parameter with the assumption 
    # it points to a JSON file that can be read and used to generate a ActuatorData instance.
    def toActuatorDataFromJsonFile(self,path):
        with open(path,"r") as json_file:
            adDict = json.load(json_file)
            ad = ActuatorData.ActuatorData()
            ad.name = adDict['name']
            ad.timeStamp = adDict['timeStamp']
            ad.command = adDict['command']
            ad.errCode = adDict['errCode']
            ad.statusCode  = adDict['statusCode']
            ad.stateData   = adDict['stateData']
            ad.val    = adDict['val']
            
            #print(" decode [post] --> " + str(ad))
            print("\n From Json File to ActuatorData: ")
            print (ad)
            return ad
 
