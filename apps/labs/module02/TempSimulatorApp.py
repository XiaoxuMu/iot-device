'''
Created on Sep 24, 2019

@author: Xiaoxu Mu
'''

# To start the thread.
import logging 
from time import sleep        
from labs.module02 import TempSensorEmulator

logging.info("Starting data processing app daemon thread...")

# If TempSensorEmulator extends from threading.Thread...
temSenEmulator = TempSensorEmulator.TempSensorEmulator()
temSenEmulator.enableEmulator = True
temSenEmulator.start()

while (True):
    sleep(10)
    pass
