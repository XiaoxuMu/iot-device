'''
Created on Sep 24, 2019

@author: Xiaoxu Mu
'''
import logging
import threading
import time 
import random
from labbenchstudios.common import SensorData
from labs.module02 import SmtpClientConnector

class TempSensorEmulator(threading.Thread):
    
    RATE_PERSEC = 10
    sensorData = SensorData.TempSensorData()                
    connector = SmtpClientConnector.SmtpClientConnector()
    curVal = sensorData.getValue()
    alterDiff = 5
    

    def __init__(self, rate = RATE_PERSEC):
        super(TempSensorEmulator, self).__init__()
        self.rate = rate
        
    # A thread to generate a temperature value between 0 C and 30 C
    def run(self):
        while True:
            if self.enableEmulator:
                temp = self.generator() 
                self.sensorData.addValue(temp)
                self.sendNotification()
                print(self.sensorData.getValue())
                print(self.getSensorData())
                           
            time.sleep(self.rate)
            
    
    def generator(self):
        return random.uniform(0.0, 30.0)
        
    def getSensorData(self):
        return self.sensorData.__str__()
        
    #While running, it will check if the randomly generated value is different from the average 
    #stored measurement by a configurable threshold (5). If the threshold is reached or surpassed, 
    #the most recent SensorData should be emailed to a remote service using the SmtpClientConnector module.    
    def sendNotification(self):
        if (abs(self.sensorData.getValue() - self.sensorData.getAvgValue()) >= self.alterDiff):
            logging.info('\n  Current temp exceeds average by > ' + str(self.alterDiff) + '. Triggering alert...')
            
            self.connector.publishMessage("Excessive Temp", self.getSensorData())
        