'''
Created on Oct 7, 2019

@author: Xiaoxu Mu
'''
# SenseHatDeviceAdaptor class implements a thread to run at a regular interval
# reading the data from the I2C buffers via the sense_hat API.
from time import sleep
import threading
from sense_hat import SenseHat

class SenseHatDeviceAdaptor(threading.Thread):
    
    RATE_PERSEC = 10
    sensehat = SenseHat() 
    
    def __init__(self, rate = RATE_PERSEC):
        super(SenseHatDeviceAdaptor, self).__init__()
        self.rate = rate
        
    def run(self):
        while True:
            if self.enableAdaptor:
                accelerometer = self.sensehat.get_accelerometer()
                magnetometer = self.sensehat.get_compass()
                pressure = self.sensehat.get_pressure()
                humidity = self.sensehat.get_humidity()
                print(' \n Accelerometer data from sense_hat API: ' + str(accelerometer))
                print(' \n Magnetometer data from sense_hat API: ' + str(magnetometer))
                print(' \n Pressure data from sense_hat API: ' + str(pressure))
                print(' \n Humidity data from sense_hat API: ' + str(humidity))
                            
            sleep(self.rate)
    
    
    