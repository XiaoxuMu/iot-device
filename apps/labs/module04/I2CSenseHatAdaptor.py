'''
Created on Oct 7, 2019

@author: Xiaoxu Mu
'''
# I2CSenseHatAdaptor class implements a thread to run at a regular interval,
# reading the data from the I2C buffers specified by the addresses for each sensor.
import smbus
import logging
import threading
from time import sleep
from labbenchstudios.common import ConfigUtil
from labbenchstudios.common import ConfigConst

# initiate i2cbus and get address from sensor.
i2cBus = smbus.SMBus(1) # Use I2C bus No.1 on Raspberry Pi3 +

enableControl = 0x2D #control command
enableMeasure = 0x08 #enable measure command

accelAddr = 0x1C # address for IMU (accelerometer)
magAddr = 0x6A # address for IMU (magnetometer)
pressAddr = 0x5C # address for pressure sensor
humidAddr = 0x5F # address for humidity sensor

begAddr = 0x28 #set up starting reading address
totBytes = 6 #set up number of bytes

DEFAULT_RATE_IN_SEC = 5

class I2CSenseHatAdaptor(threading.Thread):
    rateInSec = DEFAULT_RATE_IN_SEC
    
    def __init__(self):
        super(I2CSenseHatAdaptor, self).__init__()
        self.config = ConfigUtil.ConfigUtil(ConfigConst.DEFAULT_CONFIG_FILE_NAME)
        self.config.loadConfig() #loading config file
        print('Configuration data...\n' + str(self.config))
        self.initI2CBus() #initiate I2Cbus

    def initI2CBus(self):
        logging.info("Initializing I2C bus and enabling I2C addresses...")
        i2cBus.write_byte_data(accelAddr, 0, 0) # write accelAddr address into i2cbus
        i2cBus.write_byte_data(magAddr, 0, 0) # write magAddr address into i2cbus
        i2cBus.write_byte_data(pressAddr, 0, 0) # write pressAddr address into i2cbus
        i2cBus.write_byte_data(humidAddr, 0, 0) # write humiAddr address into i2cbus
        
    def displayAccelerometerData(self):
        # extract data from sensor by i2c and save into data
        data = i2cBus.read_i2c_block_data(accelAddr,begAddr,totBytes)
        print(' \n Accelerometer block data : ' + str(data))
        
    def displayMagnetometerData(self):
        # extract data from sensor by i2c and save into data
        data = i2cBus.read_i2c_block_data(magAddr,begAddr,totBytes)
        print(' \n Magnetometer block data : ' + str(data))
        
    def displayPressureData(self):
        # extract data from sensor by i2c and save into data
        data = i2cBus.read_i2c_block_data(pressAddr,begAddr,totBytes)
        print(' \n Pressure block data : ' + str(data))
    
    def displayHumidityData(self):
        # extract data from sensor by i2c and save into data
        data = i2cBus.read_i2c_block_data(humidAddr,begAddr,totBytes)
        print(' \n Humidity block data : ' + str(data))
    
    def hts211getHumidity(self):
        # HTS221 address, 0x5F(95)
        # Read Calibration values from non-volatile memory of the device
        # Humidity Calibration values
        # Read data back from 0x30(48), 1 byte
        val0 = i2cBus.read_byte_data(0x5F, 0x28)
        H0 = val0 / 2
        # Read data back from 0x31(49), 1 byte
        val1 = i2cBus.read_byte_data(0x5F, 0x29)
        H1 = val1 /2
        # Read data back from 0x36(54), 2 bytes
        val2 = i2cBus.read_byte_data(0x5F, 0x34)
        val3 = i2cBus.read_byte_data(0x5F, 0x35)
        H2 = ((val3 & 0xFF) * 256) + (val2 & 0xFF)
        # Read data back from 0x3A(58), 2 bytes
        val4 = i2cBus.read_byte_data(0x5F, 0x3A)
        val5 = i2cBus.read_byte_data(0x5F, 0x3B)
        H3 = ((val5 & 0xFF) * 256) + (val4 & 0xFF) 
        # Read data back from 0x28(40) with command register 0x80(128), 4 bytes
        # humidity msb, humidity lsb
        data = i2cBus.read_i2c_block_data(0x5F, 0x28)
        # Convert the data
        humidity = (data[1] * 256) + data[0]
        humidity = ((1.0 * H1) - (1.0 * H0)) * (1.0 * humidity - 1.0 * H2) / (1.0 * H3 - 1.0 * H2) + (1.0 * H0)
        # Output data to screen
        print(' \n Humidity data from HTS 221 sensor: ' + str(humidity))
        return humidity
    
    def run(self):
        while True:
            if self.enableEmulator:
                    self.displayAccelerometerData()
                    self.displayMagnetometerData()
                    self.displayPressureData()
                    self.displayHumidityData()
                    self.hts211getHumidity()
            sleep(self.rateInSec)
        
