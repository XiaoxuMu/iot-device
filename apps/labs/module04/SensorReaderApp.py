'''
Created on Sep 24, 2019

@author: Xiaoxu Mu
'''
import os,sys
sys.path.append('/home/pi/workspace/iot-device/apps')
# To start the thread.
import logging 
from time import sleep        
from labs.module04 import I2CSenseHatAdaptor
from labs.module04 import SenseHatDeviceAdaptor

logging.info("Starting SensorHAT data processing app daemon thread...")

# If I2CSenseHatAdaptor extends from threading.Thread...
i2csensehat = I2CSenseHatAdaptor.I2CSenseHatAdaptor()
i2csensehat.daemon = True
i2csensehat.enableEmulator = True
i2csensehat.start()
 
sensenhat = SenseHatDeviceAdaptor.SenseHatDeviceAdaptor()
sensenhat.enableAdaptor = True
sensenhat.start()

while (True):
    sleep(10)
    pass


