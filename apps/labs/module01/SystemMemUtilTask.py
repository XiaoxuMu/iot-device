'''
Created on Sep 14, 2019

@author: Xiaoxu Mu
'''
import psutil

class SystemMemUtilTask:

    #Get data of memory utilization percentages
    def getDataFromSensor(self):
        mem = psutil.virtual_memory()
        mempercent = mem.percent
        return mempercent 
     

         
