'''
Created on Sep 14, 2019

@author: Xiaoxu Mu
'''

import logging
import time          
import threading
from labs.module01.SystemCpuUtilTask import SystemCpuUtilTask
from labs.module01.SystemMemUtilTask import SystemMemUtilTask

class SystemPerformanceAdaptor(threading.Thread):
 
    RATE_PERSEC = 10
 
    def __init__(self, rate = RATE_PERSEC):
        super(SystemPerformanceAdaptor, self).__init__()
        self.rate = rate
    
    # A thread to get the CPU utilization percentage 
    # and virtual memory utilization percentage
    def run(self):
        while True:
            if self.enableAdaptor:
                sysCpuUtilTask = SystemCpuUtilTask()
                sysMemUtilTask = SystemMemUtilTask()
                cpuUtil  = sysCpuUtilTask.getDataFromSensor()
                memUtil  = sysMemUtilTask.getDataFromSensor()
                perfcpu = 'CPU Utilization = ' + str(cpuUtil) 
                perfmem = 'Memory Utilization = ' + str(memUtil)
                 
            logging.info(perfcpu)
            logging.info(perfmem)

            time.sleep(self.rate)
            
            

