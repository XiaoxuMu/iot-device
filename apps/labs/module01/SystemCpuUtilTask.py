'''
Created on Sep 14, 2019

@author: Xiaoxu Mu
'''
import psutil

class SystemCpuUtilTask:
    
    def __init__(self):
        pass
    
    #Get data of CPU utilization percentages
    def getDataFromSensor(self):
        cpupercent  = psutil.cpu_percent() 
        return cpupercent
    


