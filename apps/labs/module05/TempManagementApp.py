'''
Created on Sep 24, 2019

@author: Xiaoxu Mu
'''

# To start the thread.
import logging 
from time import sleep        
from labs.module05 import TempSensorAdaptor

logging.info("Starting data processing app daemon thread...")

# If TempSensorEmulator extends from threading.Thread...
temSenAdaptor = TempSensorAdaptor.TempSensorAdaptor()
temSenAdaptor.enableAdaptor = True
temSenAdaptor.start()
 
while (True):
    sleep(10)
    pass

