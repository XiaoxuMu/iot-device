'''
Created on Sep 24, 2019

@author: Xiaoxu Mu
'''

import threading
import time 
from labbenchstudios.common import SensorData
from labbenchstudios.common import DataUtil
from labs.module03 import SmtpClientConnector
from sense_hat import SenseHat
from labbenchstudios.common import ActuatorData

class TempSensorAdaptor(threading.Thread):
    
    RATE_PERSEC = 10
    sensorData = SensorData.TempSensorData() 
    actuatorData = ActuatorData.ActuatorData()
    jsondata = DataUtil.DataUtil()        
    connector = SmtpClientConnector.SmtpClientConnector()
    sensehat = SenseHat()
    curVal = sensorData.getValue()
    threshold = 2

    
    def __init__(self, rate = RATE_PERSEC):
        super(TempSensorAdaptor, self).__init__()
        self.rate = rate
        
    # A thread to get a temperature value from sensor 
    # and send the generated SensorData content as JSON to e-mail address.
    # Write the generated SensorData content as JSON to a file to the file system.
    def run(self):
        while True:
            if self.enableAdaptor:
                temp = self.sensehat.get_temperature()
                self.sensorData.addValue(temp)
                self.sensorData.pringSensorData()
                self.sendNotification()
            time.sleep(self.rate)
                   
    def getSensorData(self):
        return self.sensorData.__str__()
            
    # Check if the value read is different from the average stored measurement by a configurable threshold.
    # If the threshold is reached or surpassed, the most recent SensorData should be e-mailed to a remote service 
    # using the SmtpClientConnector module.   
    def sendNotification(self):
        if (abs(self.sensorData.getValue() - self.sensorData.getAvgValue()) >= self.threshold):
            print('\n  Current temp exceeds average by > ' + str(self.threshold) + '. Converting data...')
            self.connector.publishMessage("Temperature From Sensor", self.jsondata.toJsonFromSensorData(
                                                                     self.sensorData.name,self.sensorData.timeStamp, 
                                                                     self.sensorData.getAvgValue(),self.sensorData.getMinValue(),
                                                                     self.sensorData.getMaxValue(),self.sensorData.getValue(),
                                                                     self.sensorData.totValue,self.sensorData.sampleCount))


   
         
        