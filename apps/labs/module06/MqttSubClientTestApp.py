'''
Created on Oct 23, 2019

@author: Xiaoxu Mu
'''
# This is the Subscriber application
from labbenchstudios.common import ConfigConst
from labbenchstudios.common import ConfigUtil
import paho.mqtt.client as mqtt
from labbenchstudios.common import DataUtil

#Define variables
config = ConfigUtil.ConfigUtil('../../../config/ConnectedDevicesConfig.props')
host = config.getProperty(ConfigConst.MQTT_CLOUD_SECTION, ConfigConst.HOST_KEY)
port = int(config.getProperty(ConfigConst.MQTT_CLOUD_SECTION, ConfigConst.PORT_KEY))

#Convert data format between Json and SensorData
def convert(data):
    datautil = DataUtil.DataUtil()
    print("\nConvert Json to SensorData Instance: ")
    sensordata = datautil.toSensorDataFromJson(data)
    print("\nConvert SensorData Instance to Json: ")
    datautil.toJsonFromSensorData(sensordata.name, sensordata.timeStamp, 
                                  sensordata.avgValue, sensordata.minValue, 
                                  sensordata.maxValue, sensordata.curValue, 
                                  sensordata.totValue, sensordata.sampleCount)

#Initiate a mqtt client
mqttClient = mqtt.Client() 
 
#Define on_connect event Handler
def on_connect(client, userdata, flags, rc):
    if rc == 0:
        print("Connected to the broker. Returned code = ",rc)
    else:
        print("Connection failed. Returned code = ",rc)

#Define on_message event Handler  
def on_message(client, userdata, msg):
    message = str(msg.payload)
    jsondata = message.lstrip("b'").rstrip("'")
    print("\nTopic: " + str(msg.topic) + ". Received message: " + jsondata)
    convert(jsondata)
    mqttClient.unsubscribe(msg.topic)
    
#Define on_subscribe event Handler  
def on_subscribe(client, userdata, mid, granted_qos):
    print("Subscribed to MQTT topic.")

def on_log(client, userdata, level, buf):
    print("log: ",buf)

# Register event handlers    
mqttClient.on_message = on_message
mqttClient.on_connect = on_connect
mqttClient.on_subscribe = on_subscribe
# mqttClient.on_log = on_log

#Connect with MQTT broker
mqttClient.connect(host, port, 20)
mqttClient.subscribe("Temperature", qos = 2)

#Continue the network loop
mqttClient.loop_forever()

    
    

        
    
    
    