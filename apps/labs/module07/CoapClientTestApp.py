'''
Created on Oct 24, 2019

@author: Xiaoxu Mu
'''
# CoapClientTestApp connects to the CoAP server using CoapClientConnector,
# run a number of tests, then exit.
from labs.module07 import CoapClientConnector

Coapclient = CoapClientConnector.CoapClientConnector()
 
#Execute GET request.
Coapclient.handleGetTest("temp")

#set json payload of resource "temp".
payload = Coapclient.setTempData()
#Execute POST request.
Coapclient.handlePostTest("temp", payload)
  
#Execute PUT request.
Coapclient.handlePutTest("temp", "sensordata")
#Execute DELETE request.
Coapclient.handleDeleteTest("temp")
  
Coapclient.client.stop()









# #Execute PING request.
# Coapclient.handlePingTest()

# Print json payload of resource "temp".
# jsondata = Coapclient.client.get("temp").payload
# print("\nGET Json from CoAp Sever:\n" + jsondata)


# #Convert Json payload to Sensordata instance
# sensordata = Coapclient.convertJsontoSensordata(jsondata)
# #Convert Sensordata instance to Json
# sensordatajson = Coapclient.convertSensordatatoJson(sensordata)


