'''
Created on Oct 24, 2019

@author: Xiaoxu Mu
'''
from coapthon.client.helperclient import HelperClient
from labbenchstudios.common import ConfigConst
from labbenchstudios.common import ConfigUtil
from labbenchstudios.common import DataUtil
from labbenchstudios.common import SensorData
from coapthon.messages.message import Message
from sense_hat import SenseHat

# CoapClientConnector class is to execute the following requests from the client to the server:
# 1. Ping
# 2. GET
# 3. POST 
# 4. PUT
# 5. DELETE
class CoapClientConnector():
    #set up host, port and path  
    config = ConfigUtil.ConfigUtil('../../../config/ConnectedDevicesConfig.props')
    host = config.getProperty(ConfigConst.COAP_CLOUD_SECTION, ConfigConst.HOST_KEY )
    port = int(config.getProperty(ConfigConst.COAP_CLOUD_SECTION, ConfigConst.PORT_KEY))
    path = "temp"
        
    print('\tHost: ' + str(host))
    print('\tPort: ' + str(port))
        
    url = "coap://" + str(host) + ":" + str(port) + "/temp"
    print('Coap broker address: ' + str(url))
    
    client = HelperClient(server = (host, port))
    print("Created CoAP client reference: " + str(client))
    
    def __init__(self):
        pass
    
    # Execute GET request to server.
    def handleGetTest(self,resource):
    
        print("\nTesting GET for resource: " + str(resource))
        
        response = self.client.get(resource)
        print(response.pretty_print())
        print("GET COMPLETED!")
    
    # Execute POST request to server.
    def handlePostTest(self, resource, payload):
    
        print("\nTesting POST for resource: " + resource + ", payload: " + payload)

        response = self.client.post(resource, payload)
        print(response.pretty_print())
        print("POST COMPLETED!")
 
    # Execute PUT request to server.    
    def handlePutTest(self, resource, payload):
    
        print("\nTesting PUT for resource: " + resource + ", payload: " + payload)
        
        response = self.client.put(resource, payload)
        print(response.pretty_print())
        print("PUT COMPLETED!")

    # Execute DELETE request to server.    
    def handleDeleteTest(self, resource):
    
        print("\nTesting DELETE for resource: " + resource)
        
        response = self.client.delete(resource)
        print(response.pretty_print())
        print("DELETE COMPLETED!")
        
    #Convert Json to Sensor Data
    def convertJsontoSensordata(self, data):
        
        datautil = DataUtil.DataUtil()
        print("\nConvert Json to SensorData Instance: ")
        sensordata = datautil.toSensorDataFromJson(data)
    
        return sensordata
    
    #Convert Sensor Data to Json
    def convertSensordatatoJson(self, sensorData):

        datautil = DataUtil.DataUtil()
        print("\nConvert SensorData Instance to Json: ")
        jsonsensordata = datautil.toJsonFromSensorData(sensorData.name, sensorData.timeStamp, 
                                  sensorData.avgValue, sensorData.minValue, 
                                  sensorData.maxValue, sensorData.curValue, 
                                  sensorData.totValue, sensorData.sampleCount)
        return jsonsensordata
   
    #Using SensorData class and DataUtil class to create a simple SensorData instance
    def setSensorData(self):
        sensorData = SensorData.SensorData()
        dataUtil = DataUtil.DataUtil()
        sensehat = SenseHat()
        temp = sensehat.get_temperature()
        sensorData.addValue(temp)
        print("\nCreate Json format of Sensor data:")
        payload = dataUtil.toJsonFromSensorData(sensorData.name, sensorData.timeStamp,
                                        sensorData.avgValue, sensorData.minValue,
                                        sensorData.maxValue, sensorData.curValue,
                                        sensorData.totValue, sensorData.sampleCount)  
        return payload     
    
    
# #     Execute PING request to server.
#     def handlePingTest(self):
#  
#         print("\nTesting PING Request.")
#  
#         msg = Message()
#         msg.destination = self.host, self.port
#         msg.type = 0
#         print(msg)
#         self.client.send_empty(msg)
#         print("PING COMPLETED!")
        
        