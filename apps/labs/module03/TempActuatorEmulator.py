'''
Created on Sep 30, 2019

@author: Xiaoxu Mu
'''

from labs.module03 import TempSensorAdaptor
from labbenchstudios.common import ActuatorData
from labs.module03 import SenseHatLedActivator

#TempActuatorEmulator class is to accept actuator data.
class TempActuatorEmulator(object):
    
    # If different from its own internal instance of an ActuatorData object,
    # determine if the command from the new instance is to raise or lower the temperature, 
    # and send a notification to the SenseHAT indicating the temperature will be raised or lowered by ‘n’ degrees
    def processMessage(self, data):
    
        actuatorData = ActuatorData.ActuatorData()
        temSenAdaptor = TempSensorAdaptor.TempSensorAdaptor()
        led = SenseHatLedActivator.SenseHatLedActivator()

        temSenAdaptor.actuatorData = data
        actuatorData.updateData(temSenAdaptor.actuatorData) 
    
        if(actuatorData.stateData == 1):
            led.enableLed = True
            # Refer to the temperature will be raised by n degree
            led.displayMsg = 'Should increase ' + str(temSenAdaptor.calculate_n_degree()) + " degree. \n"
            led.start()
            led.enableLed = False
            
        else:
            if(actuatorData.stateData == -1):
                led.enableLed = True
                # Refer to the temperature will be lowered by n degree
                led.displayMsg = 'Should decrease ' + str(temSenAdaptor.calculate_n_degree()) + " degree. \n"
                led.start()
                led.enableLed = False
            else:
                led.enableLed = False    
        