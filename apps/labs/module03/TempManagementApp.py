'''
Created on Sep 24, 2019

@author: Cassie
'''

# To start the thread.
import logging 
from time import sleep        
from labs.module03 import TempSensorAdaptor

logging.info("Starting sensor and actuator processing app daemon thread...")

# If TempSensorEmulator extends from threading.Thread...
temSenAdaptor = TempSensorAdaptor.TempSensorAdaptor()
temSenAdaptor.enableAdaptor = True
temSenAdaptor.start()
 
while (True):
    sleep(10)
    pass

