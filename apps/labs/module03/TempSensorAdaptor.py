'''
Created on Sep 24, 2019

@author: Xiaoxu Mu
'''

import logging
import threading
import time 
from labbenchstudios.common import SensorData
from labs.module03 import SmtpClientConnector
from sense_hat import SenseHat
from labs.module03 import TempActuatorEmulator
from labbenchstudios.common import ActuatorData
from labbenchstudios.common import ConfigConst

class TempSensorAdaptor(threading.Thread):
    
    RATE_PERSEC = 10
    sensorData = SensorData.TempSensorData() 
    actuatorData = ActuatorData.ActuatorData()
    tempEmulator = TempActuatorEmulator.TempActuatorEmulator()          
    connector = SmtpClientConnector.SmtpClientConnector()
    sensehat = SenseHat()
    curVal = sensorData.getValue()
    threshold = 2

    
    def __init__(self, rate = RATE_PERSEC):
        super(TempSensorAdaptor, self).__init__()
        self.rate = rate
        
    # A thread to get a temperature value from sensor 
    # and  to signal a temperature increase or decrease request and
    # pass it directly to TempActuatorEmulator, invoking processMessage()
    def run(self):
        while True:
            if self.enableAdaptor:
                temp = self.sensehat.get_temperature()
                self.sensorData.addValue(temp)
                print(self.getSensorData())
                
                self.compare_nominalTemp()
                print(self.actuatorData.getActuatorData())
                self.sendNotification()
                self.tempEmulator.processMessage(self.actuatorData) 
                         
            time.sleep(self.rate)
                   
    def getSensorData(self):
        return self.sensorData.__str__()
            
    # Check if the value read is different from the average stored measurement by a configurable threshold.
    # If the threshold is reached or surpassed, the most recent SensorData should be e-mailed to a remote service 
    # using the SmtpClientConnector module.   
    def sendNotification(self):
        if (abs(self.sensorData.getValue() - self.sensorData.getAvgValue()) >= self.threshold):
            logging.info('\n  Current temp exceeds average by > ' + str(self.threshold) + '. Triggering alert...')
            self.connector.publishMessage("Temperature From Sensor", self.getSensorData())
            self.connector.publishMessage("Actuator Data", self.actuatorData.getActuatorData())
   
    #read the data of nominalTemp from ConnectedDevicesConfig.props       
    def reader(self):
        self.connector.config.nominalTemp = self.connector.config.getProperty(ConfigConst.CONSTRAINED_DEVICE, ConfigConst.NOMINAL_TEMP_KEY)  
        return int(self.connector.config.nominalTemp)
    
    #calculate the value of n to indicate the temperature will be raised or lowered by ‘n’ degrees
    def calculate_n_degree(self):
        return abs(self.sensorData.getValue() - self.reader())
    
    # Parse the SensorData content to determine if the actual temperature exceeds or falls below
    # ‘nominalTemp’ from the configuration file.      
    def  compare_nominalTemp(self):
        val = self.sensorData.getValue() - self.reader()
        if (val > 0):
            print('Current temperature exceeds by ' + str(self.reader()) + '.  Request to decrease.')
            self.actuatorData.command = 1
            self.actuatorData.statusCode = 1
            self.actuatorData.stateData = -1
            self.actuatorData.val = self.calculate_n_degree()
        else :
            if (val < 0):
                print('Current temperature falls below ' + str(self.reader()) + '.  Request to increase.')
                self.actuatorData.command = 1
                self.actuatorData.statusCode = 1
                self.actuatorData.stateData = 1
                self.actuatorData.val = self.calculate_n_degree()
            else:
                self.actuatorData.command = 0
                self.actuatorData.statusCode = 0
                self.actuatorData.stateData = 0
                self.actuatorData.val = 0.0

         
        